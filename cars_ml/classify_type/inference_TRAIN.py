from common import *
from data import *

def main():
	#-------------------------------------------------------------------
	# create data generator for training
	#-------------------------------------------------------------------
	serialize=pickle.load(open(datagenserialize,'rb'))
	trainthing=getdata(serialize=serialize,dataframe=alldata,imgdir=alldir,y_cols=y_cols,y_cat_cols=y_cat_cols,filecol='filename',shape=(height,width,channels),batch_size=32,max_queue_size=128,num_workers=6,horizontal_flip=False,shuffle=False,channel_shift=0.)
	trainthing.start(onceonly=True)

	losses=trainthing.getkeraslosses()

	model, modelage=getmodel(outputfn=trainthing.getkerasoutputs)
	if modelage=='new':
		print ( 'no trained model found' )
		quit()


	model.compile(
		loss='mse'
		,optimizer='sgd'
	)
	print ( model.summary(), flush=True )

	print ( 'filename', end='' )
	for j in range(len(y_cols)):
		c=y_cols[j]
		if c in y_cat_cols:
			print ( ',', c,',', c + '_score' ,end='' )
		else:
			print ( ',', c ,end='' )
	print ( '' )

	for X, filenames, ys in trainthing.flowfile():
		p=model.predict(X)
		for i in range(len(filenames)):
			filename=filenames[i]
			print ( filename, end='' )
			for j in range(len(y_cols)):
				c=y_cols[j]
				if c in y_cat_cols:
					cats=serialize['output_categories'][c]
					idx=np.argmax(p[j][i])
					print ( ',', cats[idx].replace(',','.'),',', p[j][i][idx] ,end='' )
				else:
					print ( ',', p[j][i][0] ,end='' )
			print ( '' )
					
	trainthing.stop()

if __name__ == "__main__":
	main()
