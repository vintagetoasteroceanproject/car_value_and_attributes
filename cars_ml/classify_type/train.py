from common import *
from data import *

def main():
	#-------------------------------------------------------------------
	# create data generator for validation
	#-------------------------------------------------------------------
	validthing=getdata(dataframe=validdata,imgdir=alldir,y_cols=y_cols,y_cat_cols=y_cat_cols,filecol='filename',shape=(height,width,channels),batch_size=validation_samples_per_epoch,shuffle=True,max_queue_size=1,num_workers=1)
	validthing.start()
	X_valid,ys_valid=next(validthing.flow())
	validthing.stop()

	#-------------------------------------------------------------------
	# create data generator for training
	#-------------------------------------------------------------------
	trainthing=getdata(serialize=validthing.serialize(),dataframe=traindata,imgdir=alldir,y_cols=y_cols,y_cat_cols=y_cat_cols,filecol='filename',shape=(height,width,channels),batch_size=32,max_queue_size=64,num_workers=6)
	trainthing.start()

	losses=trainthing.getkeraslosses()

	model, modelage=getmodel(outputfn=trainthing.getkerasoutputs)
	if modelage=='new':
		model.compile(
			loss=losses
			,optimizer=optimizer
		)
		print ( model.summary(), flush=True )

		model.fit_generator(
			trainthing.flow()
			,steps_per_epoch=int(samples_per_epoch/batch_size)
			,validation_data=[X_valid, ys_valid]
			,use_multiprocessing=False
			,workers=1
			,callbacks=callbacks
			,epochs=initial_epochs
			,max_queue_size=64
			,verbose=1
		)

	maketrainable(model,trainable=True)

	model.compile(
		loss=losses
		,optimizer=optimizer
	)
	print ( model.summary(), flush=True )

	model.fit_generator(
		trainthing.flow()
		,steps_per_epoch=int(samples_per_epoch/batch_size)
		,validation_data=[X_valid, ys_valid]
		,use_multiprocessing=False
		,workers=1
		,callbacks=callbacks
		,initial_epoch=initial_epochs
		,epochs=num_epochs-initial_epochs
		,verbose=1
		,max_queue_size=64
	)

	trainthing.stop()

if __name__ == "__main__":
	main()
