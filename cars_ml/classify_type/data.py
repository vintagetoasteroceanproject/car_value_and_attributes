from common import *
from lycon import load, resize
from copy import deepcopy as cp
from sklearn.preprocessing import OneHotEncoder

conn = pyodbc.connect(
	'Driver={ODBC Driver 17 for SQL Server};'
	'Server='+db_Server+';'
	'Database='+db_Database+';'
	'UID='+db_UID+';PWD='+db_PWD+';'
)
cursor = conn.cursor()

traindata = pd.read_sql(trainsql,conn)
validdata = pd.read_sql(validsql,conn)

print ( traindata.columns )

class getdata:
	def __init__(self,dataframe,imgdir,y_cols,y_cat_cols,serialize=None,filecol='filename',shape=(299,299,3),batch_size=32,max_queue_size=32,num_workers=2,horizontal_flip=True,shuffle=True,channel_shift=0.5):
		self.max_queue_size=max_queue_size
		self.shape=shape
		self.queue=Queue(self.max_queue_size)
		self.allstop=False
		self.num_workers=num_workers
		self.threads=[]
		self.horizontal_flip=horizontal_flip
		self.shuffle=shuffle
		self.channel_shift=channel_shift
		self.df=dataframe
		self.imgdir=imgdir
		self.y_cols=y_cols
		self.y_cat_cols=y_cat_cols
		self.batch_size=batch_size
		self.filecol=filecol
		self.encoders={}
		self.output_categories={}
		self.y_names={}
		if serialize == None:
			for c in y_cols:
				safe_name=c.replace(' ','_').replace(':','-')
				self.y_names[c]='output_'+safe_name
				try:
					input_1=Input(shape=shape,name=safe_name)
				except:
					print ( 'keras name safe version of encoded column wasn\'t safe enough', c )
					quit()
			for c in y_cat_cols:
				encoder=OneHotEncoder(sparse=False,categories='auto',handle_unknown='ignore')
				encoder.fit(self.df[[c]])
				self.encoders[c]=cp(encoder)
				self.output_categories[c]=encoder.categories_[0]
		else:
			self.y_names=serialize['y_names']
			self.encoders=serialize['encoders']
			self.output_categories=serialize['output_categories']
	def serialize(self):
		serialize={}
		serialize['y_names']=self.y_names
		serialize['encoders']=self.encoders
		serialize['output_categories']=self.output_categories
		return serialize
	def getheadings(self):
		return self.output_categories
	def getkeraslosses(self,cat_loss=focal_loss(),reg_loss='mse'):
		losses={}
		for c in y_cols:
			name=self.y_names[c]
			if c in y_cat_cols:
				losses[name]=cat_loss
			else:
				losses[name]=reg_loss
		return losses
	def getkerasoutputs(self,x):
		outputs=[]
		for c in y_cols:
			name=self.y_names[c]
			if c in y_cat_cols:
				activation='softmax'
				units=len(self.output_categories[c])
			else:
				activation='linear'
				units=1
			o=Dense(units,activation=activation,name=name)(x)
			outputs.append(o)
		return outputs
	def __get_ys(self,row):
		ys={}
		for c in y_cols:
			name=self.y_names[c]
			if c in y_cat_cols:
				a=[[row[c]]]
				ys[name]=(self.encoders[c].transform(a))
			else:
				ys[name]=(row[c])
		return ys
	def __shuffle(self):
		self.df=shuffle(self.df)
	def __loadim(self,f):
		im=load(f)
		im=resize(im, width=self.shape[1],height=self.shape[0])/255.
		return im
	def __distort(self,im):
		if self.horizontal_flip and np.random.random() < 0.5:
			im=np.fliplr(im)
		if self.channel_shift > 0. and np.random.random() < 0.5:
			for c in range(3): im[:,:,c]*=(1. - np.random.random()*self.channel_shift + np.random.random()*self.channel_shift)
		return im
	def __runthread(self,worker):
		X=np.zeros((self.batch_size,height,width,channels),dtype=np.float32)
		ys={}
		for c in y_cols:
			name=self.y_names[c]
			l=1
			if c in y_cat_cols:
				l=len(self.output_categories[c])
			ys[name]=np.zeros((self.batch_size,l),dtype=np.float32)
		i=0
		filenames=[None]*self.batch_size
		counter=0
		while self.allstop == False:
			for idx,row in self.df.iterrows():
				counter+=1
				if counter % self.num_workers == worker:
					f=row[self.filecol]
					if isfile(alldir+f) and '.jpg' in f:
						ys_row=self.__get_ys(row)
						X[i]=self.__loadim(alldir+f)
						X[i]=self.__distort(self.__loadim(alldir+f))
						for c in y_cols:
							name=self.y_names[c]
							ys[name][i,:]=ys_row[name]
						filenames[i]=f
						i+=1
						if i >= self.batch_size:
							#print ( 'about to queue' )
							self.queue.put((cp(X),cp(filenames),cp(ys)))
							#print ( 'after queue' )
							i=0
							filenames=[None]*self.batch_size
				if self.allstop:
					print ( "stopping getdata" )
					break
			if i > 0:
				self.queue.put((cp(X),cp(filenames),cp(ys)))
	def start(self):
		if self.shuffle: self.__shuffle()
		for i in range(self.num_workers):
			t=threading.Thread(target=self.__runthread, args=(i,))
			t.start()
			self.threads.append(t)
	def stop(self):
		self.allstop=True
		while not self.queue.empty():
			dummy=self.get()
		for t in self.threads:
			t.join(10)
	def __del__(self):
		self.allstop=True
		while not self.queue.empty():
			dummy=self.get()
		for t in self.threads:
			t.join(10)
	def get(self):
		return self.queue.get(block=True,timeout=30)
	def flow(self):
		while True:
			X, filenames, ys = self.queue.get(block=True,timeout=30)
			yield X, ys

