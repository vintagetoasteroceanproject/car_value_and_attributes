#-------------------------------------
# database config
#-------------------------------------
db_Server='localhost'
db_Database='classify'
db_UID='web'
db_PWD='webpassword'

#-------------------------------------
# Image and class config
#-------------------------------------
height=299
width=height
channels=3
o_features=19	#Note, this will be overridden in common after reading the data in

alldir='/data/vintage-toaster-monthly/scrapinghub/driven/201810/full/'
validpattern='%a.jpg'
holdoutpattern='%b.jpg'

#-------------------------------------
# NN params
#-------------------------------------
model_file='models/model.h5'
csvfile='logs/epochs.csv'

batch_size=32
initial_epochs=20

metrics=['accuracy']
loss='focal_loss'
optimizer='adam'
finetune_loss='focal_loss'
finetune_optimizer='adam'

use_multiprocessing=False
workers=6

num_epochs=10000
earlystop=200
samples_per_epoch=2048
validation_samples_per_epoch=2048

validation_split=0.2

#-------------------------------------
# Data config
#-------------------------------------
y_cols=['asking_price','odometer','engine_size','model','year','body_type','transmission','fuel_type','bestclass']
y_cat_cols=['model','year','body_type','transmission','fuel_type','bestclass']

trainsql="select filename, asking_price/10000.0 as asking_price, odometer/10000 as odometer, engine_size/1000 as engine_size, model, year, body_type, transmission, fuel_type, bestclass from cars_attributes where filename not like '{}' and filename not like '{}' order by newid();".format(validpattern,holdoutpattern)
validsql="select filename, asking_price/10000.0 as asking_price, odometer/10000 as odometer, engine_size/1000 as engine_size, model, year, body_type, transmission, fuel_type, bestclass from cars_attributes where filename like '{}' order by newid();".format(validpattern)


