import numpy as np
import pandas as pd
import pyodbc
from os import listdir
from os.path import isfile
from config import *
from sklearn.utils import shuffle
from sklearn.model_selection import train_test_split
from queue import Queue
import threading
import tensorflow as tf
from time import time, sleep
from sklearn.utils import shuffle

from keras import backend as K
'''
Compatible with tensorflow backend
'''
def focal_loss(gamma=2., alpha=.25):
	def focal_loss_fixed(y_true, y_pred):
		pt_1 = tf.where(tf.equal(y_true, 1), y_pred, tf.ones_like(y_pred))
		pt_0 = tf.where(tf.equal(y_true, 0), y_pred, tf.zeros_like(y_pred))
		return -K.sum(alpha * K.pow(1. - pt_1, gamma) * K.log(pt_1))-K.sum((1-alpha) * K.pow( pt_0, gamma) * K.log(1. - pt_0))
	return focal_loss_fixed

def focal_loss_fixed(y_true, y_pred):
	gamma=2.
	alpha=.25
	pt_1 = tf.where(tf.equal(y_true, 1), y_pred, tf.ones_like(y_pred))
	pt_0 = tf.where(tf.equal(y_true, 0), y_pred, tf.zeros_like(y_pred))
	return -K.sum(alpha * K.pow(1. - pt_1, gamma) * K.log(pt_1))-K.sum((1-alpha) * K.pow( pt_0, gamma) * K.log(1. - pt_0))

from keras.models import Model, Input, load_model
from keras.applications.inception_v3 import InceptionV3 #(include_top=True, weights='imagenet', input_tensor=None, input_shape=None, pooling=None, classes=1000)
from keras.layers import Dense, Conv2D, GlobalAveragePooling2D, SpatialDropout2D, Dropout, BatchNormalization as BN, Activation
from keras.preprocessing.image import ImageDataGenerator
from keras.callbacks import ModelCheckpoint #(filepath, monitor='val_loss', verbose=0, save_best_only=False, save_weights_only=False, mode='auto', period=1)
from keras.callbacks import EarlyStopping #(monitor='val_loss', min_delta=0, patience=0, verbose=0, mode='auto', baseline=None, restore_best_weights=False)
from keras.callbacks import CSVLogger #(filename, separator=',', append=False)

MySaver=ModelCheckpoint(model_file, monitor='val_loss', verbose=1, save_best_only=True, save_weights_only=False, mode='auto', period=1)
MyCSV=CSVLogger(csvfile, separator=',', append=False)
MyStopper=EarlyStopping(monitor='val_loss', min_delta=0, patience=earlystop, verbose=1, mode='auto', baseline=None, restore_best_weights=False)

callbacks=[MySaver,MyCSV,MyStopper]

def maketrainable(model,trainable=True):
	for layer in model.layers:
		layer.trainable=trainable
		if hasattr(layer,'layers'):
			for layer2 in layer.layers:
				layer2.trainable=trainable
		

def getmodel(outputfn):
	modelage='new'
	if isfile(model_file):
		modelage='existing'
		model=load_model(model_file,custom_objects={'focal_loss_fixed':focal_loss_fixed})
	else:
		basemodel=InceptionV3(include_top=False, weights='imagenet', input_shape=(height,width,3))
		maketrainable(basemodel,False)
		x=basemodel.output
		x=GlobalAveragePooling2D()(x)
		x=Dropout(0.2)(x)
		outputs=outputfn(x)

		model=Model(inputs=basemodel.inputs,outputs=outputs)

	return model, modelage
	

