from common import *

def main():
	X_valid, y_valid = next(validgen)

	model, modelage=getmodel()
	if modelage=='new':
		model.compile(
			loss=finetune_loss
			,optimizer=finetune_optimizer
			,metrics=metrics
		)
		print ( model.summary(), flush=True )

		model.fit_generator(
			traingen
			,steps_per_epoch=int(samples_per_epoch/batch_size)
			,validation_data=[X_valid, y_valid]
			,use_multiprocessing=False
			,workers=workers
			,callbacks=callbacks
			,epochs=initial_epochs
			,max_queue_size=128
			,verbose=1
		)

	maketrainable(model,trainable=True)

	model.compile(
		loss=loss
		,optimizer=optimizer
		,metrics=metrics
	)
	print ( model.summary(), flush=True )

	model.fit_generator(
		traingen
		,steps_per_epoch=int(samples_per_epoch/batch_size)
		,validation_data=[X_valid, y_valid]
		,use_multiprocessing=False
		,workers=workers
		,callbacks=callbacks
		,initial_epoch=initial_epochs
		,epochs=num_epochs-initial_epochs
		,verbose=1
		,max_queue_size=128
	)



if __name__ == "__main__":
	main()
