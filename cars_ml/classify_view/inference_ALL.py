from common import *
from scipy.misc import imread, toimage, imresize
from lycon import load, resize
from time import time, sleep
from queue import Queue
import threading
from copy import deepcopy as cp


zerothreshold=0.001

model,modelage=getmodel()

print ( 'filename,bestclass', end='' )
for c in classes:
	print ( ',', c, end='' )
print ( '', flush=True )

t=time()

class getdata:
	def __init__(self, num_workers=2):
		self.queue=Queue(64)
		self.allstop=False
		self.num_workers=num_workers
		self.threads=[]
	def __runthread(self,worker):
		X=np.zeros((batch_size,height,width,channels),dtype=np.float32)
		i=0
		filenames=[None]*batch_size
		counter=0
		for f in listdir(alldir):
			counter+=1
			if counter % self.num_workers == worker:
				if isfile(alldir+f) and '.jpg' in f:
					im=resize(load(alldir+f),width=width,height=height)/255.
					#im=imresize(imread(alldir+f),(height,width))/255.
					X[i]=im
					filenames[i]=f
					i+=1
					if i >= batch_size:
						#print ( 'about to queue' )
						self.queue.put((cp(X),cp(filenames)))
						#print ( 'after queue' )
						i=0
						filenames=[None]*batch_size
			if self.allstop:
				print ( "stopping getdata" )
				break
		if i > 0:
			self.queue.put((X,filenames))
	def start(self):
		for i in range(self.num_workers):
			t=threading.Thread(target=self.__runthread, args=(i,))
			t.start()
			self.threads.append(t)
	def stop(self):
		self.allstop=True
		for t in self.threads:
			t.join(10)
	def get(self):
		return self.queue.get(block=True,timeout=30)

def predict(model,X,filenames):
	global t
	#print ( 'before predict', time() -t )
	t=time()
	p=model.predict(X)
	#print ( 'after predict', time() -t )
	t=time()
	for i in range(len(filenames)):
		print ( filenames[i], ',', classes[np.argmax(p[i])], end='' )
		for c in p[i]:
			if c < zerothreshold: 
				c=0
			print ( ',', c, end='' )
		print ( '', flush=True )

datagen=getdata(num_workers=4)
datagen.start()
#sleep(20)

while True:
	X, filenames = datagen.get()
	predict(model,X,filenames)	

datagen.stop()
