#-------------------------------------
# database config
#-------------------------------------
db_Server='localhost'
db_Database='classify'
db_UID='web'
db_PWD='webpassword'

#-------------------------------------
# Image and class config
#-------------------------------------
height=299
width=height
channels=3
o_features=19	#Note, this will be overridden in common after reading the data in

traindir='/var/www/html/web/classify/done/'
validdir=traindir
testdir='/var/www/html/web/classify/cars/'

alldir='/data/vintage-toaster-monthly/scrapinghub/driven/201810/full/'

#-------------------------------------
# NN params
#-------------------------------------
model_file='models/model.h5'
csvfile='logs/epochs.csv'

batch_size=32
initial_epochs=20

metrics=['accuracy']
loss='focal_loss'
optimizer='adam'
finetune_loss='focal_loss'
finetune_optimizer='adam'

use_multiprocessing=False
workers=6

num_epochs=1000
earlystop=40
samples_per_epoch=2048
validation_samples_per_epoch=2048

validation_split=0.2
