import numpy as np
import pandas as pd
import pyodbc
from os import listdir
from os.path import isfile
from config import *
from sklearn.utils import shuffle
from sklearn.model_selection import train_test_split
from queue import Queue
import threading
import tensorflow as tf
from time import time, sleep

from keras import backend as K
'''
Compatible with tensorflow backend
'''
def focal_loss(gamma=2., alpha=.25):
	def focal_loss_fixed(y_true, y_pred):
		pt_1 = tf.where(tf.equal(y_true, 1), y_pred, tf.ones_like(y_pred))
		pt_0 = tf.where(tf.equal(y_true, 0), y_pred, tf.zeros_like(y_pred))
		return -K.sum(alpha * K.pow(1. - pt_1, gamma) * K.log(pt_1))-K.sum((1-alpha) * K.pow( pt_0, gamma) * K.log(1. - pt_0))
	return focal_loss_fixed

def focal_loss_fixed(y_true, y_pred):
	gamma=2.
	alpha=.25
	pt_1 = tf.where(tf.equal(y_true, 1), y_pred, tf.ones_like(y_pred))
	pt_0 = tf.where(tf.equal(y_true, 0), y_pred, tf.zeros_like(y_pred))
	return -K.sum(alpha * K.pow(1. - pt_1, gamma) * K.log(pt_1))-K.sum((1-alpha) * K.pow( pt_0, gamma) * K.log(1. - pt_0))

if loss=='focal_loss': loss=focal_loss()
if finetune_loss=='focal_loss': finetune_loss=focal_loss()

conn = pyodbc.connect(
	'Driver={ODBC Driver 17 for SQL Server};'
	'Server='+db_Server+';'
	'Database='+db_Database+';'
	'UID='+db_UID+';PWD='+db_PWD+';'
)
cursor = conn.cursor()


sql = "select filename, class from cars where class != 'discard';"
sql = "select filename, class from cars;"
data = pd.read_sql(sql,conn)
dummies = pd.get_dummies(data,columns=['class'],prefix='',prefix_sep='')

print ( data.columns )
classes=dummies.columns[1:].values.tolist()
print ( 'classes:', classes, type(classes) )

traindata, validdata = train_test_split(data, test_size=validation_split, random_state=42)
validdata=validdata.reset_index(drop=True)
traindata=traindata.reset_index(drop=True)

o_features=len(classes)

from keras.models import Model, Input, load_model
from keras.applications.inception_v3 import InceptionV3 #(include_top=True, weights='imagenet', input_tensor=None, input_shape=None, pooling=None, classes=1000)
from keras.layers import Dense, Conv2D, GlobalAveragePooling2D, SpatialDropout2D, Dropout, BatchNormalization as BN, Activation
from keras.preprocessing.image import ImageDataGenerator
from keras.callbacks import ModelCheckpoint #(filepath, monitor='val_loss', verbose=0, save_best_only=False, save_weights_only=False, mode='auto', period=1)
from keras.callbacks import EarlyStopping #(monitor='val_loss', min_delta=0, patience=0, verbose=0, mode='auto', baseline=None, restore_best_weights=False)
from keras.callbacks import CSVLogger #(filename, separator=',', append=False)

MySaver=ModelCheckpoint(model_file, monitor='val_loss', verbose=1, save_best_only=True, save_weights_only=False, mode='auto', period=1)
MyCSV=CSVLogger(csvfile, separator=',', append=False)
MyStopper=EarlyStopping(monitor='val_loss', min_delta=0, patience=earlystop, verbose=1, mode='auto', baseline=None, restore_best_weights=False)

callbacks=[MySaver,MyCSV,MyStopper]

def maketrainable(model,trainable=True):
	for layer in model.layers:
		layer.trainable=trainable
		if hasattr(layer,'layers'):
			for layer2 in layer.layers:
				layer2.trainable=trainable
		

def getmodel():
	modelage='new'
	if isfile(model_file):
		modelage='existing'
		model=load_model(model_file,custom_objects={'focal_loss_fixed':focal_loss_fixed})
	else:
		basemodel=InceptionV3(include_top=False, weights='imagenet', input_shape=(height,width,3))
		maketrainable(basemodel,False)
		x=basemodel.output
		x=GlobalAveragePooling2D()(x)
		x=Dropout(0.2)(x)
		output_1=Dense(o_features,activation='softmax')(x)

		model=Model(inputs=basemodel.inputs,outputs=output_1)

	return model, modelage
	

testdatagen = ImageDataGenerator(
	rescale=1./255
)
datagen = ImageDataGenerator(
	rescale=1./255
	,rotation_range=10
	,zoom_range=0.1
	#,shear_range=0.1
	,channel_shift_range=0.1
	,horizontal_flip=True
)

class parallel_panda_gen:
	def __init__(self,df, imgdir, imagegen, classes, batch_size=32,num_workers=4,max_queue_size=64, x_col='filename', y_col='class', target_size=(299,299),rndsleep=0.0625
):
		self.queue=Queue(max_queue_size)
		self.generators=[]
		self.threads=[]
		self.allstop=False
		self.num_workers=num_workers
		self.rndsleep=rndsleep
		for i in range(num_workers):
			self.generators.append(
				imagegen.flow_from_dataframe(
					df,imgdir,x_col=x_col,y_col=y_col,has_ext=True,target_size=target_size,color_mode='rgb',classes=classes,class_mode='categorical',batch_size=batch_size,shuffle=True,seed=i
				)
			)
	def __threadgen(self,generator, i):
		for X,y in generator:
			self.queue.put((X,y))
			if self.allstop:
				break
			if self.num_workers > 0 and self.rndsleep > 0.:
				sleep(np.random.random()*self.rndsleep)
	def start(self):
		for i in range(len(self.generators)):
			print ( 'starting worker thread', i )
			t=threading.Thread(target=self.__threadgen, args=(self.generators[i],i))
			t.start()
			self.threads.append(
				t
			)
	def stop(self):
		self.allstop=True
		for t in self.threads:
			self.queue.empty()
			t.join(1)

	def flow(self):
		while True:
			yield self.queue.get()

	def __del__(self):
		self.stop()
		
			

traingen=datagen.flow_from_dataframe(traindata, traindir, x_col='filename', y_col='class', has_ext=True, target_size=(height, width), color_mode='rgb', classes=classes, class_mode='categorical', batch_size=batch_size, shuffle=True)
validgen=datagen.flow_from_dataframe(validdata, validdir, x_col='filename', y_col='class', has_ext=True, target_size=(height, width), color_mode='rgb', classes=classes, class_mode='categorical', batch_size=validation_samples_per_epoch, shuffle=True)


#prlgen=parallel_panda_gen(traindata,traindir,validdatagen,classes=classes,batch_size=batch_size,num_workers=workers, rndsleep=0., max_queue_size=64)
#prlgen.start()

"""
counter=0
t0=time()
diversity={}
for X,y in prlgen.flow():
#for X,y in traingen:
	print ( X.shape, y.shape, counter )
	for x in X:
		key=str(np.std(x))
		if key in diversity:
			diversity[key]+=1
		else:
			diversity[key]=1
	counter+=1
	if counter >= 32:
		break

print ( diversity )
print ( 'Time in seconds:', time() - t0 )
print ( 'diversity:', len(diversity) )
	
prlgen.stop()
"""
