from common import *
from scipy.misc import imread, toimage, imresize

model,modelage=getmodel()

X=np.zeros((batch_size,height,width,3),dtype=np.float32)

def predict(model,X,filenames):
	p=model.predict(X)
	for i in range(len(filenames)):
		print ( filenames[i], classes[np.argmax(p[i])] )	
		cursor.execute("delete from cars_QA where filename = ?", filenames[i])
		cursor.commit()
		cursor.execute("insert into cars_QA(filename,class) values ( ?, ? )", filenames[i],classes[np.argmax(p[i])])
		cursor.commit()

i=0
filenames=[]
for f in listdir(testdir):
	if isfile(testdir+f) and '.jpg' in f:
		im=imresize(imread(testdir+f),(height,width))/255.
		X[i]=im
		filenames.append(f)
		i+=1
		if i >= batch_size:
			predict(model,X,filenames)
			i=0
			filenames=[]
if i > 0:
	predict(model,X,filenames)
		
