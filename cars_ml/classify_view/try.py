from common import *

def main():

	model, modelage=getmodel()
	model.compile(
		loss=finetune_loss
		,optimizer=finetune_optimizer
		,metrics=metrics
	)
	print ( model.summary() )

	X_valid, y_valid = next(validgen)

	model.fit(
		X_valid, y_valid
	)


if __name__ == "__main__":
	main()
