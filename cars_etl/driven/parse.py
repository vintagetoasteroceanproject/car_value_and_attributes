import json
from os import listdir
from os.path import isfile
from shutil import move

import pymssql

server = 'MUM-LAPTOP\DAVIDSQL'
user = 'loaddata'
password = 'loaddata'
db = 'loadData'

def s(s): 
    return "'"+str(s).replace(',',"',").replace("'","''")+"'"
def sqldo(s):
    cursor.execute(s)
    conn.commit()
def sqlget(s):
    cursor.execute(s)
    return cursor.fetchone()

conn = pymssql.connect(server, user, password, db)
cursor = conn.cursor()

datadir='data/'
processdir=datadir+'Processing/'
archivedir=datadir+'Archive/'

collist=['asking_price', 'description', 'title', 'contact_number', 'financing_options', 'make', 'model', 'year', 'engine_size', 'odometer', 'exterior_colour', 'transmission', 'fuel_type', 'location', 'fuel_economy', 'stock_number', 'url', 'body_type']

for f in listdir(datadir):
    if isfile(datadir+f):
        #move(datadir+f,processdir+f)
        processdir=datadir

        infile=open(processdir+f,'r')
        data=json.load(infile)
        print (len(data))

        sqldo('insert into drivenCars.filecontrol(filename) values( \''+f+'\' )')
        fileid=sqlget('select SCOPE_IDENTITY()')[0]

        print ( fileid )

        """
        #Print Columns
        for d in data:
            for key in d:
                if key not in collist:
                    collist.append(key)

        print ( collist )
        """

        counter=0
        for d in data:
            counter+=1
            print ( counter )
            images=[]
            sqlcols='insert into drivenCars.Advert(fileid'
            sqlvals='values('+s(fileid)
            for key in d:
                if key == 'images':
                    images=d[key]
                if key in collist:
                    sqlcols+=',['+key+']'
                    sqlvals+=','+s(d[key])


            sqlcols+=')'
            sqlvals+=')'
            sqldo(sqlcols+sqlvals)
            advertid=sqlget('select SCOPE_IDENTITY()')[0]
            for img in images:
                url=img['url']
                path=img['path']
                sqldo('insert into drivenCars.AdvertImage(advertid,url,path) values('+s(advertid)+','+s(url)+','+s(path)+')')

        sqldo('update drivenCars.filecontrol set recordCount='+str(counter)+', loaded=1 where fileid=\''+str(fileid)+'\'')

print ( 'done.' )
