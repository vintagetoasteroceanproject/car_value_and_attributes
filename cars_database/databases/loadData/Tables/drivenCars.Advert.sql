SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [drivenCars].[Advert] (
		[fileid]                [bigint] NOT NULL,
		[AdvertID]              [bigint] IDENTITY(1, 1) NOT NULL,
		[asking_price]          [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[description]           [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[title]                 [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[contact_number]        [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[financing_options]     [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[make]                  [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[model]                 [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[year]                  [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[engine_size]           [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[odometer]              [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[exterior_colour]       [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[transmission]          [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[fuel_type]             [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[location]              [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[fuel_economy]          [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[stock_number]          [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[url]                   [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[body_type]             [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[createdOn]             [datetimeoffset](7) NULL,
		[ExternalKey]           [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK__Advert__4FE88F2431D69F9A]
		PRIMARY KEY
		CLUSTERED
		([AdvertID])
	ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [drivenCars].[Advert]
	ADD
	CONSTRAINT [DF__Advert__createdO__5D95E53A]
	DEFAULT (sysdatetimeoffset()) FOR [createdOn]
GO
ALTER TABLE [drivenCars].[Advert]
	WITH CHECK
	ADD CONSTRAINT [FK__Advert__fileid__5CA1C101]
	FOREIGN KEY ([fileid]) REFERENCES [drivenCars].[filecontrol] ([fileid])
	ON DELETE CASCADE
ALTER TABLE [drivenCars].[Advert]
	CHECK CONSTRAINT [FK__Advert__fileid__5CA1C101]

GO
CREATE NONCLUSTERED INDEX [advert_idx2]
	ON [drivenCars].[Advert] ([ExternalKey], [fileid])
	ON [PRIMARY]
GO
ALTER TABLE [drivenCars].[Advert] SET (LOCK_ESCALATION = TABLE)
GO
