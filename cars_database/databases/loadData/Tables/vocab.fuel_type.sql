SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [vocab].[fuel_type] (
		[fuel_type]           [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[fuel_type_group]     [varchar](18) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[samples]             [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [vocab].[fuel_type] SET (LOCK_ESCALATION = TABLE)
GO
