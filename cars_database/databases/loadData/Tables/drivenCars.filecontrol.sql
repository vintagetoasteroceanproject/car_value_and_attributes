SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [drivenCars].[filecontrol] (
		[fileid]                [bigint] IDENTITY(1, 1) NOT NULL,
		[filename]              [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[comments]              [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[derived_data_date]     [datetimeoffset](7) NULL,
		[createdOn]             [datetimeoffset](7) NULL,
		[createdBy]             [varchar](8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[recordCount]           [bigint] NULL,
		[loaded]                [bit] NULL,
		CONSTRAINT [PK__filecont__C2C7C244FBA44B72]
		PRIMARY KEY
		CLUSTERED
		([fileid])
	ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [drivenCars].[filecontrol]
	ADD
	CONSTRAINT [DF__filecontr__creat__57DD0BE4]
	DEFAULT (sysdatetimeoffset()) FOR [createdOn]
GO
ALTER TABLE [drivenCars].[filecontrol]
	ADD
	CONSTRAINT [DF__filecontr__creat__58D1301D]
	DEFAULT (suser_sname()) FOR [createdBy]
GO
ALTER TABLE [drivenCars].[filecontrol]
	ADD
	CONSTRAINT [DF__filecontr__loade__59C55456]
	DEFAULT ((0)) FOR [loaded]
GO
ALTER TABLE [drivenCars].[filecontrol] SET (LOCK_ESCALATION = TABLE)
GO
