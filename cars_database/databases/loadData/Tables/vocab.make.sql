SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [vocab].[make] (
		[make]          [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[makegroup]     [nvarchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [vocab_make_idx1]
	ON [vocab].[make] ([make])
	ON [PRIMARY]
GO
ALTER TABLE [vocab].[make] SET (LOCK_ESCALATION = TABLE)
GO
