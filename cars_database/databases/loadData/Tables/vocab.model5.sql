SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [vocab].[model5] (
		[makegroup]     [varchar](800) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[model]         [varchar](800) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [vocab_model5_idx1]
	ON [vocab].[model5] ([makegroup], [model])
	ON [PRIMARY]
GO
ALTER TABLE [vocab].[model5] SET (LOCK_ESCALATION = TABLE)
GO
