SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [drivenCars].[AdvertImage] (
		[AdvertID]     [bigint] NOT NULL,
		[ImageID]      [bigint] IDENTITY(1, 1) NOT NULL,
		[url]          [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[path]         [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK__AdvertIm__7516F4ECD7E6E227]
		PRIMARY KEY
		CLUSTERED
		([ImageID])
	ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [drivenCars].[AdvertImage]
	WITH CHECK
	ADD CONSTRAINT [FK__AdvertIma__Adver__607251E5]
	FOREIGN KEY ([AdvertID]) REFERENCES [drivenCars].[Advert] ([AdvertID])
	ON DELETE CASCADE
ALTER TABLE [drivenCars].[AdvertImage]
	CHECK CONSTRAINT [FK__AdvertIma__Adver__607251E5]

GO
ALTER TABLE [drivenCars].[AdvertImage] SET (LOCK_ESCALATION = TABLE)
GO
