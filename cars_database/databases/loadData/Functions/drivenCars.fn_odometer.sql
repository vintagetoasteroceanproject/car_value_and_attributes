SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
create function drivenCars.fn_odometer(@odometer varchar(1000)) returns float
as
begin
return try_convert(float,replace(replace(replace(@odometer,'kms',''),'''',''),',',''))
end
GO
