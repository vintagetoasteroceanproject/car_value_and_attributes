USE [loadData]
GO
CREATE USER [loaddata] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_owner] ADD MEMBER [loaddata]
GO
