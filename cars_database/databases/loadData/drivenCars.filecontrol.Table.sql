USE [loadData]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [drivenCars].[filecontrol](
	[fileid] [bigint] IDENTITY(1,1) NOT NULL,
	[filename] [varchar](max) NULL,
	[comments] [varchar](max) NULL,
	[derived_data_date] [datetimeoffset](7) NULL,
	[createdOn] [datetimeoffset](7) NULL,
	[createdBy] [varchar](8000) NULL,
	[recordCount] [bigint] NULL,
	[loaded] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[fileid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [drivenCars].[filecontrol] ADD  DEFAULT (sysdatetimeoffset()) FOR [createdOn]
GO
ALTER TABLE [drivenCars].[filecontrol] ADD  DEFAULT (suser_sname()) FOR [createdBy]
GO
ALTER TABLE [drivenCars].[filecontrol] ADD  DEFAULT ((0)) FOR [loaded]
GO
