USE [loadData]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [drivenCars].[RemoveOrphans]
as
	delete 
	from drivenCars.filecontrol
	where loaded=0
GO
