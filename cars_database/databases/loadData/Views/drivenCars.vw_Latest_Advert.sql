SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
create view drivenCars.vw_Latest_Advert
as
select *
from (
	select 
		*
		,ROW_NUMBER() over(partition by ExternalKey order by fileid desc, createdOn desc) as rownum
	from drivenCars.Advert as a with(nolock)
) as tmp
where tmp.rownum=1
GO
