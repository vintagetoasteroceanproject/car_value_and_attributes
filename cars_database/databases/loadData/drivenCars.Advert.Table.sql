USE [loadData]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [drivenCars].[Advert](
	[fileid] [bigint] NOT NULL,
	[AdvertID] [bigint] IDENTITY(1,1) NOT NULL,
	[asking_price] [varchar](max) NULL,
	[description] [varchar](max) NULL,
	[title] [varchar](max) NULL,
	[contact_number] [varchar](max) NULL,
	[financing_options] [varchar](max) NULL,
	[make] [varchar](max) NULL,
	[model] [varchar](max) NULL,
	[year] [varchar](max) NULL,
	[engine_size] [varchar](max) NULL,
	[odometer] [varchar](max) NULL,
	[exterior_colour] [varchar](max) NULL,
	[transmission] [varchar](max) NULL,
	[fuel_type] [varchar](max) NULL,
	[location] [varchar](max) NULL,
	[fuel_economy] [varchar](max) NULL,
	[stock_number] [varchar](max) NULL,
	[url] [varchar](max) NULL,
	[body_type] [varchar](max) NULL,
	[createdOn] [datetimeoffset](7) NULL,
PRIMARY KEY CLUSTERED 
(
	[AdvertID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [drivenCars].[Advert] ADD  DEFAULT (sysdatetimeoffset()) FOR [createdOn]
GO
ALTER TABLE [drivenCars].[Advert]  WITH CHECK ADD FOREIGN KEY([fileid])
REFERENCES [drivenCars].[filecontrol] ([fileid])
ON DELETE CASCADE
GO
