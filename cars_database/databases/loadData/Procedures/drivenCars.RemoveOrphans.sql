SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [drivenCars].[RemoveOrphans]
as
	delete 
	from drivenCars.filecontrol
	where loaded=0

	delete
	from drivenCars.Advert
	where fileid not in (select fileid from drivenCars.filecontrol)

	delete
	from drivenCars.AdvertImage
	where AdvertID not in (select AdvertID from drivenCars.Advert)
GO
