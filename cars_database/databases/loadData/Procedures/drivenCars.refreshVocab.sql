SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
create procedure drivenCars.refreshVocab
as

begin try drop table vocab.fuel_type end try begin catch end catch

select fuel_type
	,case
		when fuel_type like '%hybrid%' then 'hybrid'
		when fuel_type like '%electric%' then 'electric'
		when fuel_type like '%diesel%' then 'diesel'
		when fuel_type like '%91%' then 'petrol'
		when fuel_type like '%unlead%' then 'petrol'
		when fuel_type like '%95%' then 'petrol high octane'
		when fuel_type like '%96%' then 'petrol high octane'
		when fuel_type like '%97%' then 'petrol high octane'
		when fuel_type like '%98%' then 'petrol high octane'
		when fuel_type like '%premium%' then 'petrol high octane'
		when fuel_type like '%petrol%' then 'petrol'
		else 'NULL'
	end as fuel_type_group
	,count(*) as samples
into vocab.fuel_type
from [drivenCars].[vw_Latest_Advert]
group by fuel_type
order by count(*) desc
GO
