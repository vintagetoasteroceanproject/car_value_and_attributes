USE [classify]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--truncate table cars_type_inference
create view [dbo].[results]
as
select 
	a.AdvertID
	,a.asking_price
	,avg(i.asking_price*10000) as estimated_price
from cars_type_inference as i
inner join cars_attributes as a on a.filename=i.filename
where i.bestclass in (
	'front'
	,'side'
	,'back'
	,'side rear'
	,'side front'
)
group by a.AdvertID, a.asking_price
GO
