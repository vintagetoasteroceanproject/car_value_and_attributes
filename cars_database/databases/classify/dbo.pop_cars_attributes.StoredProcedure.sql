USE [classify]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[pop_cars_attributes]
as

	begin try drop table #temp1 end try begin catch end catch
	begin try drop table #engine_size end try begin catch end catch
	begin try drop table #odometer end try begin catch end catch
	begin try drop table cars_attributes end try begin catch end catch

	select 
		try_convert(float,replace(replace(replace(a.asking_price,'$',''),'''',''),',','')) as asking_price
		,try_convert(float,replace(replace(replace(a.odometer,'kms',''),'''',''),',','')) as odometer
		,try_convert(float,replace(replace(replace(a.engine_size,'cc',''),'''',''),',','')) as engine_size
		,mo.makegroup+':'+mo.model as model
		,y.year
		,b.body_type
		--,i.path
		,a.AdvertID
		,t.transmission
		,f.fuel_type
	into #temp1
	from loadData.drivenCars.Advert as a
	inner join loadData.vocab.make as m on a.make=m.make 
	inner join loadData.vocab.model5 as mo on mo.makegroup=m.makegroup and a.model=mo.model
	inner join loadData.vocab.[year] as y on y.year=a.year
	inner join loadData.vocab.body_type as b on b.body_type=a.body_type
	inner join loadData.vocab.transmission as t on t.transmission=a.transmission
	inner join loadData.vocab.fuel_type as f on f.fuel_type=a.fuel_type
	--inner join loadData.[drivenCars].[AdvertImage] as i on i.AdvertID=a.AdvertID

	select distinct
		model,year,body_type,
		PERCENTILE_CONT(0.5) within group (order by engine_size) over (partition by model,year,body_type) as engine_size
	into #engine_size
	from #temp1
	where engine_size is not null

	select distinct
		model,year,body_type,
		PERCENTILE_CONT(0.5) within group (order by odometer) over (partition by model,year,body_type) as odometer
	into #odometer
	from #temp1
	where odometer is not null

	update t
	set t.engine_size=e.engine_size
	from #temp1 as t
	inner join #engine_size as e on e.model=t.model and e.year=t.year and e.body_type=t.body_type
	where t.engine_size is null

	update #temp1
	set engine_size=1800
	where engine_size is null

	update t
	set t.odometer=o.odometer
	from #temp1 as t
	inner join #odometer as o on o.model=t.model and o.year=t.year and o.body_type=t.body_type
	where t.odometer is null

	update #temp1
	set odometer=50000
	where odometer is null

	begin try drop table cars_attributes end try begin catch end catch

	select distinct t.*, replace(i.path,'full/','') as filename, ci.bestclass
	into cars_attributes
	from #temp1 as t
	inner join loadData.drivenCars.AdvertImage as i on i.AdvertID=t.advertID
	inner join classify.dbo.cars_inference as ci on ci.filename=replace(i.path,'full/','')



GO
