USE [classify]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cars_type_inference](
	[filename] [varchar](1000) NULL,
	[asking_price] [float] NULL,
	[odometer] [float] NULL,
	[engine_size] [float] NULL,
	[model] [varchar](1000) NULL,
	[model_score] [float] NULL,
	[year] [varchar](1000) NULL,
	[year_score] [float] NULL,
	[body_type] [varchar](1000) NULL,
	[body_type_score] [float] NULL,
	[transmission] [varchar](1000) NULL,
	[transmission_score] [float] NULL,
	[fuel_type] [varchar](1000) NULL,
	[fuel_type_score] [float] NULL,
	[bestclass] [varchar](1000) NULL,
	[bestclass_score] [float] NULL
) ON [PRIMARY]
GO
