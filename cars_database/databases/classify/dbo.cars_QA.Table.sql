USE [classify]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cars_QA](
	[filename] [nvarchar](4000) NULL,
	[class] [varchar](1000) NULL,
	[qa] [varchar](10) NULL,
	[created] [datetime] NULL,
	[updated] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [cars_QA_idx1] ON [dbo].[cars_QA]
(
	[filename] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[cars_QA] ADD  DEFAULT (getdate()) FOR [created]
GO
