USE [classify]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cars_inference](
	[filename] [varchar](1000) NULL,
	[bestclass] [varchar](1000) NULL,
	[back] [float] NULL,
	[boot] [float] NULL,
	[dashboard or steering] [float] NULL,
	[discard] [float] NULL,
	[engine] [float] NULL,
	[front] [float] NULL,
	[gearlever] [float] NULL,
	[inside drivers] [float] NULL,
	[inside front] [float] NULL,
	[inside gadgets] [float] NULL,
	[inside rear] [float] NULL,
	[inside rear looking forward] [float] NULL,
	[side] [float] NULL,
	[side front] [float] NULL,
	[side rear] [float] NULL,
	[wheel] [float] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [cars_inference_idx1] ON [dbo].[cars_inference]
(
	[filename] ASC,
	[bestclass] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [cars_inference_idx2] ON [dbo].[cars_inference]
(
	[bestclass] ASC
)
INCLUDE ( 	[filename]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
