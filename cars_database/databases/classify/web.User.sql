USE [classify]
GO
CREATE USER [web] FOR LOGIN [web] WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_datareader] ADD MEMBER [web]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [web]
GO
