USE [classify]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cars_attributes](
	[asking_price] [float] NULL,
	[odometer] [float] NULL,
	[engine_size] [float] NULL,
	[model] [varchar](1601) NULL,
	[year] [varchar](max) NULL,
	[body_type] [varchar](max) NULL,
	[AdvertID] [bigint] NOT NULL,
	[transmission] [varchar](max) NULL,
	[fuel_type] [varchar](max) NULL,
	[filename] [varchar](max) NULL,
	[bestclass] [varchar](1000) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
