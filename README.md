# Cars value and attribute estimator #

Given photos of cars (from auction site adverts) predict the asking price, make, model, body type, transmission, fuel type and age.

### demo ###

* The contents of the cars_web/demo/ folder can be downloaded and run
* requirements:
	* python
	* hug
	* tensorflow 1.14
	* pandas
	* pillow (PIL)
	* requests
	* urllib
	* scikit-learn
* Assuming windows, run the "run.cmd" file in a command window
* you'll see something like this:
* ![](demo.png)

### Dev/Rough status ###

* not to be taken internally!

### What is this for? ###

* Explore my love AVMs in a non-real-estate target to avoid conflict with my(then) comployer.
* Experiment with a single Neural Network model with multiple outputs and a mix of scalar and classification targets.

```
Single Neural Network with multiple outputs.
Trained on 3 months worth of scraped data from a New Zealand Auctions website with data from late 2018.
The model is an inception v3 base, additional classification layers transfer learning and then finetuned to the new targets.
The outputs are:
Scalar outputs: ['asking_price','odometer','engine_size']
Catagorical outputs:
['model','year','body_type','transmission','fuel_type','bestclass'] 

The loss function for the categorical outputs was focal loss
The loss function for the scalar outputs was mean squared error.
Using the Adam optimizer.
Implmented using Keras 1.14.0 with a tensorflow backend.
	
```
### Training ###
	* there are various web-pages in the cars_web folder used for training. 
	* loosely this is an active learning project
	* This repository should be complete but the work flow is non-intuitive with many manual steps
	* The neural network training and inference code is in the cars_ml folder
	* Some of the sub-folders and items are obsolete. Apologies! This is not a production ready project. Hobby only!

### Who do I talk to? ###

* david.knox@gmail.com
