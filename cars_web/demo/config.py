#-------------------------------------
# database config
#-------------------------------------
db_Server='localhost'
db_Database='classify'
db_UID='web'
db_PWD='webpassword'

#-------------------------------------
# Image and class config
#-------------------------------------
height=299
width=height
channels=3
o_features=19	#Note, this will be overridden in common after reading the data in

alldir='/data/vintage-toaster-monthly/scrapinghub/driven/201810/full/'

#-------------------------------------
# file params
#-------------------------------------
model_file='models/model.h5'
publish_model_file='models/PublishCarMulti.h5'
csvfile='logs/epochs.csv'
datagenserialize='models/datagenserialize.pkl'

#-------------------------------------
# NN params
#-------------------------------------
batch_size=32
initial_epochs=20
use_multiprocessing=False
workers=6
num_epochs=10000
earlystop=200
samples_per_epoch=2048
validation_samples_per_epoch=2048
optimizer='adam'

#-------------------------------------
# Data config
#-------------------------------------
y_cols=['asking_price','odometer','engine_size','model','year','body_type','transmission','fuel_type','bestclass']
y_mult={'asking_price':10000,'odometer':10000,'engine_size':1000}
y_cat_cols=['model','year','body_type','transmission','fuel_type','bestclass']

trainsql="select filename, asking_price/10000.0 as asking_price, odometer/10000 as odometer, engine_size/1000 as engine_size, model, year, body_type, transmission, fuel_type, bestclass from cars_attributes_train order by newid()";
validsql="select filename, asking_price/10000.0 as asking_price, odometer/10000 as odometer, engine_size/1000 as engine_size, model, year, body_type, transmission, fuel_type, bestclass from cars_attributes_valid order by newid()";
allsql="select filename, asking_price/10000.0 as asking_price, odometer/10000 as odometer, engine_size/1000 as engine_size, model, year, body_type, transmission, fuel_type, bestclass from cars_attributes";


