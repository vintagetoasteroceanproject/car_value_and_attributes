import hug
import requests
from io import BytesIO
from PIL import Image
from urllib.parse import urlsplit
from tensorflow.keras.models import load_model
import pickle
import numpy as np
from sklearn.preprocessing import OneHotEncoder
from sklearn import preprocessing
from config import *
from copy import deepcopy as cp
from os.path import isfile

def getX(n): return np.zeros((n,299,299,3),dtype=np.float32), []
def accumulate(p,H):
	for i in range(len(p)):
		for j in range(len(p[i])):
			for k in range(len(p[i][j])):
				H[i][j][k]+=p[i][j][k]
			
	

html = hug.get(output=hug.output_format.html)

#------------------------------------------------------------------------
# HTML templates
#------------------------------------------------------------------------
with open(r'html\predict.html', 'r') as myfile: indexdoc = myfile.read()
with open(r'html\resulthead.html', 'r') as myfile: resultheaddoc = myfile.read()
with open(r'html\resultdetail.html', 'r') as myfile: resultdetaildoc = myfile.read()

imdir='c:\\inetpub\\wwwroot\\img\\'

def p2json(p,filenames):
	out=''
	for i in range(len(filenames)):
		filename=filenames[i]
		print ( filename )
		out=filename
		for j in range(len(y_cols)):
			c=y_cols[j]
			print ( c )
			if c in y_cat_cols:
				cats=headings['output_categories'][c]
				idx=np.argmax(p[j][i])
				#print ( ',', cats[idx].replace(',','.'),',', p[j][i][idx] ,end='' )
				out+=','+cats[idx].replace(',','.')+','+str(p[j][i][idx])
			else:
				#print ( ',', p[j][i][0] ,end='' )
				out+=','+str(p[j][i][0])
	return out

def headerString(p,d=1.0):
	d=float(d)
	i=0
	out=''
	for j in range(len(y_cols)):
		c=y_cols[j]
		if c != 'bestclass':
			if c in y_cat_cols:
				cats=headings['output_categories'][c]
				idx=np.argmax(p[j][i])
				#print ( ',', cats[idx].replace(',','.'),',', p[j][i][idx] ,end='' )
				out+='<tr><th>'+c+'</th><td>'+cats[idx]+' <small>('+str(p[j][i][idx]/d)+')</small></td></tr>'
				"""
				if c == 'model':
					for x in range(3):
						p[j][i][idx]=0
						idx=np.argmax(p[j][i])
						out+='<tr><th>'+c+'</th><td>'+cats[idx]+' <small>('+str(p[j][i][idx]/d)+')</small></td></tr>'
				"""
			else:
				#print ( ',', p[j][i][0] ,end='' )
				value=p[j][i][0]/d
				if c in y_mult:
					value*=y_mult[c]
					value=str(int(value))
					if c == "asking_price":
						value='$'+value
				out+='<tr><th>'+c+'</th><td>'+value+'</td></tr>'
	return out
def p2row(p,filenames):
	out=''
	for i in range(len(filenames)):
		filename=filenames[i]
		print ( filename )
		out='<td><img class="thumbnail" src="http://localhost/img/'+filename+'" /></td>'
		idx=np.argmax(p[y_cols.index('bestclass')][i])
		view=headings['output_categories']['bestclass'][idx]
		idx=np.argmax(p[y_cols.index('model')][i])
		model=headings['output_categories']['model'][idx]
		#out+='<td>'+str(view)+'<small> ('+str(p[i][y_cols.index['bestclass'][idx])+') </small></td>'
		out+='<td>'+str(view)+'</td>'
		out+='<td>'+model+'</td>'
		"""
		for j in range(len(y_cols)):
			c=y_cols[j]
			print ( c )
			if c in y_cat_cols:
				cats=headings['output_categories'][c]
				idx=np.argmax(p[j][i])
				#print ( ',', cats[idx].replace(',','.'),',', p[j][i][idx] ,end='' )
				out+='<td>'+cats[idx]+'<small>('+str(p[j][i][idx])+')</small></td>'
			else:
				#print ( ',', p[j][i][0] ,end='' )
				value=p[j][i][0]
				if c in y_mult:
					value*=y_mult[c]
					value=int(value)
				out+='<td>'+str(value)+'</td>'
		"""
	return '<tr>'+out+'</tr>'


imgdir='img/'

if 'headings' not in globals():
	headings=pickle.load(open('datagenserialize.pkl','rb'))

if 'model' not in globals():
	print ( 'loading keras model' )
	model=load_model('PublishCarMulti.h5')

@hug.get('/getimg')
@hug.post('/getimg')
def getimgs(imgurl):
	p,filenames=requests_image(imgurl)
	if p != False:
		msg=p2row(p,filenames)
	else:
		msg='not saved successfully'
	return ( p,msg )

@hug.get('/multi')
@hug.post('/multi')
def multi(urls):
	urls=urls.split(';')
	msg=[]
	for imgurl in urls:
		msg.append(getimgs(imgurl))
	return msg

def requests_image(file_url):
	suffix_list = ['bmp', 'jpg', 'gif', 'png', 'tif']
	file_name =  urlsplit(file_url)[2].split('/')[-1]
	file_suffix = file_name.split('.')[1]
	if file_suffix in suffix_list:
		#print ( 'requests image url:', file_url, file_name, file_suffix, i.status_code )
		if not isfile(imdir+file_name):
			i = requests.get(file_url)
			if file_suffix in suffix_list and i.status_code == requests.codes.ok:
				img = Image.open(BytesIO(i.content)).resize(size=(299,299))
				img.save(imdir+file_name)
			else:
				return False
		else:
			img = Image.open(imdir+file_name)
		img = np.array(img)/255.
		print ( img.shape, np.mean(img) )
		X,filenames=getX(1)
		filenames.append(file_name)
		X[0,:,:,:]=img
		p=model.predict(X)
		print ( p )

		return p, filenames
	else:
		return False

def htmlreplace(doc,content):
	newdoc=cp(doc)
	print ( 'content', content )
	for key in content:
		stuff=content[key]
		search='<!--'+key+'-->'
		#print ( key, stuff, search )
		newdoc=newdoc.replace(search,stuff)
	return newdoc
		

@html.get('/')
@html.post('/')
def index(**kwargs):
	content={}
	msg=''
	counturls=0
	if kwargs is not None:
		resultdetail=''
		for key in kwargs:
			value=kwargs[key]
			msg+="\n"+str(key)+'='+str(value)
			if "url" in key:
				p,newresultdetail=getimgs(value)
				resultdetail+=newresultdetail
				if counturls == 0:
					H=cp(p)
				else:
					accumulate(p,H)
				counturls+=1
		
		if counturls > 0:
			resultdetail=htmlreplace(resultdetaildoc,{'ROWS':'<tbody>'+resultdetail+'</tbody>'})
			resultheader=htmlreplace(resultheaddoc,{'PRE':headerString(H,counturls)})
			content['RESULTDETAIL']=resultdetail
			content['RESULTHEAD']=resultheader
				
	content['DEBUG']=msg
	return htmlreplace(indexdoc,content)
	
