<?php

	session_start();

	$thispage='viewer.php';

	$indir='cars/';
	$donedir='done/';

	$conn = odbc_connect("Driver={ODBC Driver 17 for SQL Server};Server=localhost;Database=classify;", 'web', 'webpassword');
?>
<html>
	<head>
		<style>
			body {background-color:rgb(240,240,240);font-size:16px;font-family:verdana,sans;}
			.searchpage {display:block;height:100vh;vertical-align:center;text-align:center;}
			.searchpos {line-height:90vh;}
			input {font-size:20px;font-family:verdana,sans;}
			select {font-size:20px;font-family:verdana,sans;}
			.checkbox {border-radius:5px;height:20px;width:20px;}
			.show {width:640px;margin:0px;padding:0px;}
			.results {width:680px;margin:10px;padding:10px;border-style:solid;border-radius:15px;border-width:2px;background-color:white;}
			.menu {position:absolute;right:50px;top:50px;background-color:rgb(200,255,200);padding:10px;border-radius:15px;border-style:solid;border-color:rgb(150,255,150);border-width:2px;}
		</style>
	</head>
	<body>
		<form>
		<?php include("menu.php");
		if ( $_GET['table'] ) {
			$table=$_GET['table'];
			if ( $_GET['limit'] ) { $limit=$_GET['limit']; }
			if ( ! $limit ) {
				$limit=100;
			}
			if ( $_GET['random'] ) { $random=$_GET['random']; }
			if ( ! $random ) {
				$random='ordered';
			}

			print ( '<div class="menu">
					<p><a href="'.$thispage.'">clear</a></p>
					<p>
						<input name="table" type="text" value="'.$table.'" placeholder="Table to view">table</input>
						<input name="menugo" type="submit" value="Go!" />
					</p>
					<p> <input name="limit" type="text" value="'.$limit.'">limit rows</input> </p>
					<p>
					<select name="random">
						<option name="'.$random.'" value="'.$random.'" selected >'.$random.'</option>
						<option name="ordered" value="ordered" >ordered</option>
						<option name="random" value="random" >random</option>
					</select> </p>
			');
			$collist=[];
			if ( $_SESSION['collist'] ) {
				$collist=$_SESSION['collist'];
			} else {
				$sqlcmd='select bestclass from '.$table.' group by bestclass order by count(*) desc';
				$result=odbc_exec($conn,$sqlcmd);
				while ( $row = odbc_fetch_array($result) ) { 
					$col=$row['bestclass'];
					array_push($collist,$col);
				}
			}
			print ( '<table><tr><th>class</th><th>+ve</th><th>-ve</th></tr>' );
			$sqlcols='';
			$colcount=0;
			foreach ($collist as $col) {
				$checked='';
				$not_checked='';
				if ( $_GET[str_replace(' ','_',$col)] ) {
					$checked=' checked';
					$sqlcols.='+['.$col.']';
					$colcount=$colcount+1;
				}
				if ( $_GET['not_'.str_replace(' ','_',$col)] ) {
					$not_checked=' checked';
					$sqlcols.='+1-['.$col.']';
					$colcount=$colcount+1;
				}
				print ( '<tr><td>'.$col.'</td><td><input type="checkbox" name="'.$col.'" '.$checked.' class="checkbox" /></td>' );
				print ( '<td><input type="checkbox" name="not_'.$col.'" '.$not_checked.' class="checkbox" /></td></td>' );
			}
			$sqlcols='('.$sqlcols.')/'.$colcount;
			print ( '</table></div>' );
			$_SESSION['collist']=$collist;
			if ( $_GET['menugo'] ) {
				$orderby='newid()';
				if ( $random == 'ordered' ) {
					$orderby=$sqlcols.' desc ';
				}
				$sqlcmd='
					select
						filename, bestclass, score
					from (
						select top '.$limit.'
							filename
							,bestclass
							,'.$sqlcols.' as score
						from cars_inference
						order by '.$orderby.'
					) as tmp
					order by score desc
				';
        			$result=odbc_exec($conn,$sqlcmd);
			        while ( $row = odbc_fetch_array($result) ) {
					$score=$row['score'];
					$color='rgb(0,255,0)';
					if ( $score > 0.9 ) {
						$color='rgb(0,255,0)';
					} elseif ( $score > 0.7 ) {
						$color='rgb(50,205,0)';
					} elseif ( $score > 0.5 ) {
						$color='rgb(100,155,0)';
					} elseif ( $score > 0.3 ) {
						$color='rgb(150,105,0)';
					} elseif ( $score > 0.1 ) {
						$color='rgb(200,55,0)';
					} elseif ( $score >= 0.0 ) {
						$color='rgb(255,0,0)';
					}
					print ( '<div class="results" style="border-color:'.$color.';"><table>' );
					print ( '<tr><td colspan=2><img src="full/'.$row['filename'].'"class="show" /></td></tr>' );
					print ( '<tr><td>Best Class:'.$row['bestclass'].'</td><td>Score: '.$score.'</td></tr>' );
					print ( '</table></div>' );
				}


			}

		} else {
			print ( '<div class="searchpage">
					<span class="searchpos">
					<input name="table" type="text" value="" placeholder="Table to view" />
					<input type="submit" value="Go!" />
					</span>
				</div>'
			);
		}
		print ( '<pre>' );
		print_r( $_GET );
		print_r( $_SESSION );
		print_r ( $sqlcmd );
		print ( '</pre>' );
		?>
		</form>
	</body>
</html>
