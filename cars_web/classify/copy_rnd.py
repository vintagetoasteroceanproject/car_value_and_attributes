from os import listdir
from sklearn.utils import shuffle
from shutil import copy

indir='/data/vintage-toaster-monthly/scrapinghub/driven/201810/full/'

filelist=shuffle(listdir(indir))[:5000]

for f in filelist:
    if '.jpg' in f:
        copy(indir+f,'cars/'+f)
